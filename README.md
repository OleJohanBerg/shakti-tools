Brief introduction about the repostory
======================================

This repository hosts a set of toolchain binaries prebuilt on Ubuntu 18.04. These prebuilt binaries support application development on shakti class of processors.

For ease of use, the RISC-V toolchain is pre-built and hosted in this repository. If you want to build the toolchain yourself, please click [here](https://gitlab.com/shaktiproject/software/riscv-tools).


Minor Details
===================

1. Master branch hosts the pre-built binaries for Ubuntu 18.04.

2. Incase, you don't have a ubuntu 18.04 operating system. Please click [here](https://gitlab.com/shaktiproject/software/riscv-tools).

3. Incase, you face a download issue. Please build toolchain using [riscv-tools](https://gitlab.com/shaktiproject/software/riscv-tools).

Insight into pre-built binaries:
=====================================

1. Full Bare-metal riscv gcc toolchain with gdb for rv64.
2. Full Bare-metal riscv gcc toolchain with gdb for rv32.
3. openocd (jtag/bitbang supported)
4. Multib enabled
5. elf2hex convertor
6. spike simulator
7. Proxy Kernel (pk)
8. Toolchain build with Medany model

note: The compilers are baremetal compilers

## Toolchain download steps

Please download the tools based on your requirement.

### Download and install prebuilt tool chain binaries (copy and paste on terminal)

```
git clone --recursive https://gitlab.com/shaktiproject/software/shakti-tools.git
cd shakti-tools
SHAKTITOOLS=$PWD
export PATH=$PATH:$SHAKTITOOLS/bin:$SHAKTITOOLS/riscv64/bin:$SHAKTITOOLS/riscv32/bin
export PATH=$PATH:$SHAKTITOOLS/riscv64/riscv64-unknown-elf/bin:$SHAKTITOOLS/riscv32/riscv32-unknown-elf/bin
```

### Download only 64 bit gnu toolchain, spike and riscv-pk (copy and paste on terminal) 

```
git clone https://gitlab.com/shaktiproject/software/shakti-tools.git
cd shakti-tools/riscv64
git submodule update --init --recursive
SHAKTITOOLS=$PWD
export PATH=$PATH:$SHAKTITOOLS/bin:$SHAKTITOOLS/riscv64/bin
export PATH=$PATH:$SHAKTITOOLS/riscv64/riscv64-unknown-elf/bin
```

### Download only 32 bit gnu toolchain, spike and riscv-pk (copy and paste on terminal) 

```
git clone https://gitlab.com/shaktiproject/software/shakti-tools.git
cd shakti-tools/riscv32
git submodule update --init --recursive
SHAKTITOOLS=$PWD
export PATH=$PATH:$SHAKTITOOLS/bin:$SHAKTITOOLS/riscv32/bin
export PATH=$PATH:$SHAKTITOOLS/riscv32/riscv32-unknown-elf/bin
```

Note:

* Please copy the last 3 lines in download steps and paste them in the .bashrc file in the home folder.

* The $SHAKTITOOLS is the location of the repository. 

* The command *which riscv64-unknown-elf-gcc* helps you to verify the export of toolchain.


Please raise issues using [issues](https://gitlab.com/shaktiproject/software/shakti-tools/issues) menu.